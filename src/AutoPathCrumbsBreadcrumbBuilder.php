<?php

/**
 * @file
 * Contains \Drupal\autopathcrumbs\AutoPathCrumbsBreadcrumbBuilder.
 */

namespace Drupal\autopathcrumbs;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Routing\LinkGeneratorTrait;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class to define the menu_link breadcrumb builder.
 */
class AutoPathCrumbsBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    // Apply always.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $links = [];
    $menu_tree = \Drupal::menuTree();

    $current_path = \Drupal::service('path.current')->getPath();
    // We want the alias if there is one.
    // If there is not this will return the path again anyway.
    $current_path = \Drupal::service('path.alias_manager')->getAliasByPath($current_path);

    // Menus to check.
    // @todo: Make this configurable.
    $menu_names = array(
      'main',
      'secondary-menu',
    );

    // Iterate over the path stripping off the last part each time.
    // Starting at char 1 to avoid the leading slash.
    $pos = strrpos($current_path, '/', 1);
    while ($pos !== FALSE) {
      // Strip the slash and everything after.
      $current_path = substr($current_path, 0, $pos);
      // Get the URL for this path.
      $url = \Drupal::service('path.validator')->getUrlIfValid($current_path);
      if ($url !== FALSE) {
        // Get the route name for this URL.
        if ($route_name = $url->getRouteName()) {
          foreach ($menu_names as $menu_name) {
            // Load menu items for this menu that are for this route.
            $parameters = new MenuTreeParameters();
            $parameters->addCondition('route_name', $route_name);
            $menu_items = $menu_tree->load($menu_name, $parameters);

            // If we have a matching route then add a link for it to the crumbs.
            if ($menu_items) {
              // Just take the first one.
              $menu_item = reset($menu_items);
              $links[] = Link::createFromRoute($menu_item->link->getTitle(), $route_name);
              // If we have one break out of the foreach.
              break;
            }
          }
        }
      }
      // Check if there is another level.
      $pos = strrpos($current_path, '/', 1);
    }

    // Add the home link.
    // @todo: This could potentially be configurable.
    $links[] = Link::createFromRoute($this->t('Home'), '<front>');

    // The links are all in the reverse order so reverse them.
    $links = array_reverse($links);

    // Generate the Breadcrumb object.
    $breadcrumb = new Breadcrumb();
    $breadcrumb->setLinks($links);
    $breadcrumb->addCacheContexts(['route']);
    return $breadcrumb;
  }
}
